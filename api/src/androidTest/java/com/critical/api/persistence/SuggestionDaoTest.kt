package com.critical.api.persistence

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.critical.api.ResUtils
import com.critical.here.persistence.HereDataBase
import com.critical.here.persistence.SuggestionEntity
import com.google.gson.reflect.TypeToken
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class SuggestionDaoTest {
    inline fun <reified T : Any> getType() = object : TypeToken<T>() {}.type!!

    private lateinit var hereDataBase: HereDataBase
    private lateinit var suggestions : List<SuggestionEntity>

    @Before
    fun initDb() {
        suggestions = ResUtils().getMockObject<List<SuggestionEntity>>(this, "entities.json", getType<List<SuggestionEntity>>())!!
        hereDataBase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                                                    HereDataBase::class.java).build()
        for (suggestion in suggestions) {
            hereDataBase.suggestionDao().insert(suggestion)
        }
    }

    @After
    fun closeDb() {
        hereDataBase.close()
    }

    @Test
    fun insertFavorites() {
        hereDataBase.suggestionDao().getAllSuggestions()
                .subscribeOn(Schedulers.trampoline())
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertValue { longArray -> longArray.size == 4 }
                .assertValueCount(1)
    }


    @Test
    fun isFavorites() {
        hereDataBase.suggestionDao().count("NT_GJrcUvtYhQ.QacqHboDVyB")
                .subscribeOn(Schedulers.trampoline())
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertValue { count -> count == 1 }
                .assertValueCount(1)
    }

    @Test
    fun isNotFavorites() {
        hereDataBase.suggestionDao().count("122132")
                .subscribeOn(Schedulers.trampoline())
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertValue { count -> count == 0 }
                .assertValueCount(1)
    }

    @Test
    fun delete() {
        val entity = suggestions[0]!!

        hereDataBase.suggestionDao().delete(entity)
        hereDataBase.suggestionDao().count(entity.locationId!!)
                .subscribeOn(Schedulers.trampoline())
                .test()
                .awaitDone(5, TimeUnit.SECONDS)
                .assertNoErrors()
                .assertValue { count -> count == 0 }
                .assertValueCount(1)
    }

}