package com.critical.api

import com.google.gson.Gson
import java.lang.reflect.Type

class ResUtils {


    fun getMockData(clazz: Any, path: String): String {
        val inputStream = clazz.javaClass.classLoader.getResourceAsStream(path)
        if (inputStream != null) {
            return inputStream.reader(Charsets.UTF_8).readText()
        }
        return ""
    }


    fun <T> getMockObject(clazz: Any, path: String, type: Type): T? {
        val data: String = getMockData(clazz, path)
        return Gson().fromJson<T>(data, type)
    }

}