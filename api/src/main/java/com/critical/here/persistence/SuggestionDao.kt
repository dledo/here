package com.critical.here.persistence

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface SuggestionDao {

    @Query("SELECT * from suggestion_table")
    fun getAllSuggestions(): Flowable<List<SuggestionEntity>>

    @Query("SELECT COUNT(*) FROM suggestion_table WHERE location_id == :id")
    fun count(id: String): Flowable<Int>

    @Insert(onConflict = REPLACE)
    fun insert(suggestion: SuggestionEntity)

    @Delete
    fun delete(suggestion: SuggestionEntity)


}