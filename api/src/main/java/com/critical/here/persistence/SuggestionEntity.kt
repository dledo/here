package com.critical.here.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.critical.here.remote.geocoder.DisplayPosition
import com.critical.here.remote.models.Address

@Entity(tableName = "suggestion_table")
class SuggestionEntity constructor(@NonNull locationId: String, label: String, distance: Int, matchLevel: String,
                                   country: String, state: String, county: String, city: String, district: String, street: String, postalCode: String,
                                   latitude: Double, longitude: Double) {

    constructor(@NonNull locationId: String, label: String, distance: Int, matchLevel: String,
                address: Address, displayPosition: DisplayPosition) : this(
            locationId, label, distance, matchLevel,
            address.country.toString(), address.state.toString(), address.county.toString(), address.city.toString(),
            address.district.toString(), address.street.toString(), address.postalCode.toString(),
            displayPosition.latitude!!, displayPosition.longitude!!)

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "location_id")
    var locationId: String = ""

    @ColumnInfo(name = "label")
    var label: String? = null

    @ColumnInfo(name = "distance")
    var distance: Int = 0

    @ColumnInfo(name = "match_level")
    var matchLevel: String? = null

    //Address info
    @ColumnInfo(name = "country")
    var country: String? = null

    @ColumnInfo(name = "state")
    var state: String? = null

    @ColumnInfo(name = "county")
    var county: String? = null

    @ColumnInfo(name = "city")
    var city: String? = null

    @ColumnInfo(name = "district")
    var district: String? = null

    @ColumnInfo(name = "street")
    var street: String? = null

    @ColumnInfo(name = "postalCode")
    var postalCode: String? = null

    //Display Postion
    @ColumnInfo(name = "latitude")
    var latitude: Double = 0.0

    @ColumnInfo(name = "longitude")
    var longitude: Double = 0.0

    //_____

    init {
        this.locationId = locationId
        this.label = label
        this.distance = distance
        this.matchLevel = matchLevel

        this.country = country
        this.state = state
        this.county = county
        this.city = city
        this.district = district
        this.street = street
        this.postalCode = postalCode

        this.latitude = latitude
        this.longitude = longitude
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SuggestionEntity

        if (locationId != other.locationId) return false
        if (label != other.label) return false
        if (distance != other.distance) return false
        if (matchLevel != other.matchLevel) return false

        return true
    }

    override fun hashCode(): Int {
        var result = locationId?.hashCode() ?: 0
        result = 31 * result + (label?.hashCode() ?: 0)
        result = 31 * result + (distance ?: 0)
        result = 31 * result + (matchLevel?.hashCode() ?: 0)
        return result
    }


}