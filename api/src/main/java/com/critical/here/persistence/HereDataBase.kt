package com.critical.here.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context


const val DATABASE_NAME = "here-db"

@Database(entities = [SuggestionEntity::class], version = 1)
abstract class HereDataBase : RoomDatabase() {

    abstract fun suggestionDao(): SuggestionDao

    companion object {
        @Volatile
        private var instance: HereDataBase? = null

        @JvmStatic
        fun get(context: Context): HereDataBase {
            return instance ?: synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, HereDataBase::class.java, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()
                this.instance = instance
                instance
            }
        }
    }

}