package com.critical.here.remote.geocoder

import com.google.gson.*
import java.lang.reflect.Type

/**
 * Class to handle with custom Session object deserialization and serialization
 */

class DisplayPositionAdapter : JsonDeserializer<DisplayPosition> {


    @Throws(JsonParseException::class)
    override fun deserialize(jsonElement: JsonElement?, type: Type, jsonDeserializationContext: JsonDeserializationContext): DisplayPosition? {
        if (jsonElement != null && jsonElement.isJsonObject) {
            val jsonObject = jsonElement.asJsonObject
            if (jsonObject != null && jsonObject.has("Response")) {
                val response = jsonObject.getAsJsonObject("Response")
                if (response != null && response.has("View")) {
                    val views = response.getAsJsonArray("View")
                    if (views != null && views.size() > 0) {
                        val view = views.get(0).asJsonObject
                        if (view != null && view.has("Result")) {
                            val results = view.getAsJsonArray("Result")
                            if (results != null && results.size() > 0) {
                                val result = results.get(0).asJsonObject
                                if (result.has("Location")) {
                                    val locationData = result.getAsJsonObject("Location")
                                    if (locationData != null) {
                                        val displayPositionData = locationData.getAsJsonObject("DisplayPosition");
                                        if(displayPositionData != null){
                                            return Gson().fromJson(displayPositionData, DisplayPosition::class.java)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null
    }
}
