package com.critical.here.remote.models;

import java.io.Serializable;

/**
 * Generic error object to be used on SdK
 */

public final class RequestError extends Throwable implements Serializable {

    private static final String TAG = "RequestError";

    public enum Type {
        /**
         * Invoked when a network exception occurred talking to the server or when an unexpected
         * exception occurred creating the request or processing the response.
         */
        NETWORK,
        /**
         * Invoked for a received HTTP response with code outside of range 200-300
         */
        HTTP,
        /**
         * Unknown error reason
         */
        OTHER
    }

    private final Type mType;
    private int mCode;
    private String mMsg;
    private String mBody;
    private Throwable mThrowable;
    private String mErrorsBodyRawString;
    private String mUrl;


    public RequestError(final String url, final int errorCode, final String msg) {
        mType = Type.HTTP;
        mUrl = url;
        mCode = errorCode;
        mMsg = msg;
    }

    public RequestError(final Type type, final Throwable throwable) {
        if (throwable instanceof RequestError) {
            mType = ((RequestError) throwable).mType;
            mUrl = ((RequestError) throwable).mUrl;
            mCode = ((RequestError) throwable).mCode;
            mMsg = ((RequestError) throwable).mMsg;
            mErrorsBodyRawString = ((RequestError) throwable).mErrorsBodyRawString;
        } else {
            mType = type;
        }
        mThrowable = throwable;
    }

    public RequestError(final Type type, final Throwable throwable, final String msg) {
        if (throwable instanceof RequestError) {
            mType = ((RequestError) throwable).mType;
            mUrl = ((RequestError) throwable).mUrl;
            mCode = ((RequestError) throwable).mCode;
            mMsg = ((RequestError) throwable).mMsg;
            mErrorsBodyRawString = ((RequestError) throwable).mErrorsBodyRawString;
        } else {
            mType = type;
            mMsg = msg;
        }
        mThrowable = throwable;
    }


    public final void setErrorsBodyRaw(final String errorsBodyRaw) {
        mErrorsBodyRawString = errorsBodyRaw;
    }


    public final Type getType() {
        return mType;
    }

    public final int getCode() {
        return mCode;
    }

    public final Throwable getException() {
        return mThrowable;
    }

    public final String getBody() {
        return mBody;
    }

    public final String getMsg() {
        return mMsg;
    }

    public final String getUrl() {
        return mUrl;
    }

    public final String getErrorsBodyRawString() {
        return mErrorsBodyRawString;
    }

}
