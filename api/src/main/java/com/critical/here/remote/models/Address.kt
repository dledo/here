package com.critical.here.remote.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Address : Serializable {

    @SerializedName(value = "country")
    @Expose
    val country: String? = null

    @SerializedName("state")
    @Expose
    val state: String? = null

    @SerializedName("county")
    @Expose
    val county: String? = null

    @SerializedName("city")
    @Expose
    val city: String? = null

    @SerializedName("district")
    @Expose
    val district: String? = null

    @SerializedName("street")
    @Expose
    val street: String? = null

    @SerializedName("houseNumber")
    @Expose
    val houseNumber: String? = null

    @SerializedName("unit")
    @Expose
    val unit: String? = null

    @SerializedName("postalCode")
    @Expose
    val postalCode: String? = null

}