package com.critical.here.remote.models.autocomplete

import com.critical.here.remote.models.Address
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Suggestion : Serializable {


    @SerializedName("label")
    @Expose
    val label: String? = null

    @SerializedName("language")
    @Expose
    val language: String? = null

    @SerializedName("countryCode")
    @Expose
    val countryCode: String? = null


    @SerializedName("locationId")
    @Expose
    val locationId: String = ""

    @SerializedName("address")
    @Expose
    val address: Address? = null

    @SerializedName("distance")
    @Expose
    val distance: Int = 0

    @SerializedName("matchLevel")
    @Expose
    val matchLevel: String? = null
}