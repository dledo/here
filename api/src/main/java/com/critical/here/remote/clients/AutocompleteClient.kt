package com.critical.here.remote.clients

import android.support.annotation.VisibleForTesting
import com.critical.here.remote.Config
import com.critical.here.remote.services.Autocomplete
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

const val AUTOCOMPLETE_BASE_URL = "http://autocomplete.geocoder.api.here.com"
const val AUTOCOMPLETE_VERSION = "6.2"

@VisibleForTesting
class AutocompleteClient constructor(logType: Config.LogType, timeoutMillis: Long, authInterceptor: Interceptor) {

    private val mService: Autocomplete

    init {
        val mHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        mHttpClientBuilder.connectTimeout(timeoutMillis, TimeUnit.MILLISECONDS)
        mHttpClientBuilder.readTimeout(timeoutMillis, TimeUnit.MILLISECONDS)
        mHttpClientBuilder.writeTimeout(timeoutMillis, TimeUnit.MILLISECONDS)
        mHttpClientBuilder.pingInterval(0, TimeUnit.MILLISECONDS)
        mHttpClientBuilder.retryOnConnectionFailure(false)

        // Add the logger as last so that it can also log the interceptor modifications
        val logger = HttpLoggingInterceptor()
        when (logType) {
            Config.LogType.FULL -> logger.level = HttpLoggingInterceptor.Level.BODY
            Config.LogType.BASIC -> logger.level = HttpLoggingInterceptor.Level.HEADERS
            else -> logger.level = HttpLoggingInterceptor.Level.NONE
        }
        mHttpClientBuilder.addInterceptor(logger)
        mHttpClientBuilder.addInterceptor(authInterceptor)

        // Rest Adapter
        val retrofit: Retrofit
        val restAdapter = Retrofit.Builder()
                .client(mHttpClientBuilder.build())
                .baseUrl(String.format(Locale.US, "%s/%s/", AUTOCOMPLETE_BASE_URL, AUTOCOMPLETE_VERSION))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())


        retrofit = restAdapter.build()
        mService = retrofit.create(Autocomplete::class.java)

    }


    fun getService(): Autocomplete {
        return mService
    }
}
