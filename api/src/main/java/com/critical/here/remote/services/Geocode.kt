package com.critical.here.remote.services

import com.critical.here.remote.geocoder.DisplayPosition
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Geocode {

    @GET("geocode.json")
    fun geoPosition(@Query("locationid") locId: String): Single<DisplayPosition>


}
