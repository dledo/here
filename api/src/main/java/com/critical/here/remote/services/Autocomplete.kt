package com.critical.here.remote.services

import com.critical.here.remote.models.autocomplete.Suggestions
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Autocomplete {

    @GET("suggest.json")
    fun suggestions(@Query("query") q: String): Single<Suggestions>

    @GET("suggest.json")
    fun suggestions(@Query("query") q: String,
                    @Query("prox") latLong: String?): Single<Suggestions>

}
