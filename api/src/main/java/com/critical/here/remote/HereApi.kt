package com.critical.here.remote

import com.critical.here.remote.clients.AutocompleteClient
import com.critical.here.remote.clients.GeocodeClient
import com.critical.here.remote.geocoder.DisplayPosition
import com.critical.here.remote.models.autocomplete.Suggestions
import io.reactivex.Single
import okhttp3.Interceptor

const val APP_ID_KEY = "app_id"
const val APP_CODE_KEY = "app_code"

open class HereApi constructor(config: Config?) {

    private var mAutocompleteClient: AutocompleteClient
    private var mGeocodeClient: GeocodeClient

    init {
        if (config == null) {
            throw NullPointerException("Sdk Config cannot be null")
        }

        val authInterceptor = Interceptor { chain ->
            var request = chain.request()
            val urlBuilder = request.url().newBuilder().also {
                it.addQueryParameter(APP_ID_KEY, config.appId)
                it.addQueryParameter(APP_CODE_KEY, config.appCode)
            }
            request = request.newBuilder().url(urlBuilder.build()).build()
            chain.proceed(request)
        }

        // CREATE API CLIENTS
        mAutocompleteClient = AutocompleteClient(config.mLogType, config.mTimeout, authInterceptor)
        mGeocodeClient = GeocodeClient(config.mLogType, config.mTimeout, authInterceptor)
    }


    open fun loadGeoPosition(locationId: String): Single<DisplayPosition> {
        return mGeocodeClient.getService().geoPosition(locationId)
    }


    open fun searchSuggestions(query: String, latLong: String?): Single<Suggestions> {
        return mAutocompleteClient.getService().suggestions(query, latLong)
    }

    companion object {
        @Volatile
        private var sInstance: HereApi? = null
        private var sConfig: Config? = null

        @JvmStatic
        val instance: HereApi
            get() {
                var result = sInstance
                if (result == null) {
                    synchronized(HereApi::class.java) {
                        result = sInstance
                        if (result == null) {
                            result = HereApi(sConfig)
                            sInstance = result
                        }
                    }
                }
                return result!!
            }

        @JvmStatic
        fun create(configuration: Config) {
            sConfig = configuration
        }


    }


}
