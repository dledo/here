package com.critical.here.remote


class Config( val appId: String,  val appCode: String, val timeout: Long) {

    val mAppId = appId
    val mAppCode = appCode
    val mTimeout = timeout
    var mLogType = LogType.BASIC


    enum class LogType {
        NONE, BASIC, FULL
    }
}
