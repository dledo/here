package com.critical.here.remote.models.autocomplete

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Suggestions {

    @SerializedName("suggestions")
    @Expose
    val items: List<Suggestion>? = null

}