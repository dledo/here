package com.critical.here.remote.clients

import android.support.annotation.VisibleForTesting
import com.critical.here.remote.Config
import com.critical.here.remote.geocoder.DisplayPosition
import com.critical.here.remote.geocoder.DisplayPositionAdapter
import com.critical.here.remote.services.Geocode
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

const val GEOCODE_BASE_URL = "http://geocoder.api.here.com"
const val GEOCODE_VERSION = "6.2"

@VisibleForTesting
class GeocodeClient constructor(logType: Config.LogType, timeoutMillis: Long,
                                authInterceptor: Interceptor) {

    private val mService: Geocode

    init {
        val mHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        mHttpClientBuilder.connectTimeout(timeoutMillis,
                                          TimeUnit.MILLISECONDS)
        mHttpClientBuilder.readTimeout(timeoutMillis,
                                       TimeUnit.MILLISECONDS)
        mHttpClientBuilder.writeTimeout(timeoutMillis,
                                        TimeUnit.MILLISECONDS)
        mHttpClientBuilder.pingInterval(0,
                                        TimeUnit.MILLISECONDS)
        mHttpClientBuilder.retryOnConnectionFailure(false)

        // Add the logger as last so that it can also log the interceptor modifications
        val logger = HttpLoggingInterceptor()
        when (logType) {
            Config.LogType.FULL -> logger.level = HttpLoggingInterceptor.Level.BODY
            Config.LogType.BASIC -> logger.level = HttpLoggingInterceptor.Level.HEADERS
            else -> logger.level = HttpLoggingInterceptor.Level.NONE
        }
        mHttpClientBuilder.addInterceptor(logger)
        mHttpClientBuilder.addInterceptor(authInterceptor)

        // Rest Adapter
        val retrofit: Retrofit
        val restAdapter = Retrofit.Builder().client(mHttpClientBuilder.build())
                .baseUrl(String.format(Locale.US,
                                       "%s/%s/",
                                       GEOCODE_BASE_URL,
                                       GEOCODE_VERSION))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory
                                             .create(GsonBuilder().registerTypeAdapter(DisplayPosition::class.java, DisplayPositionAdapter())
                                                             .create()))


        retrofit = restAdapter.build()
        mService = retrofit.create(Geocode::class.java)

    }


    fun getService(): Geocode {
        return mService
    }
}
