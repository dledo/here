package com.critical.here.remote.geocoder

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DisplayPosition constructor(jsonData : JsonObject) {

    @SerializedName("Latitude")
    @Expose
    var latitude: Double = 0.0


    @SerializedName("Longitude")
    @Expose
    var longitude: Double = 0.0

    init {
        latitude = jsonData.get("Latitude").asDouble
        longitude = jsonData.get("Longitude").asDouble
    }

}