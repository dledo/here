package com.critical.api

import com.critical.here.remote.Config
import com.critical.here.remote.HereApi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test

class HereApiTest {

    @Test
    fun `test initialization`(){

        val config = Config("appId", "appCode", 300000)

        assertEquals("appId",config.mAppId)
        assertEquals("appCode",config.mAppCode)
        assertEquals(300000,config.mTimeout)
        assertEquals(Config.LogType.BASIC, config.mLogType)

        HereApi.create(config)

        assertNotNull(HereApi.instance)

    }
}