package com.critical.api.client

import com.critical.here.remote.Config
import com.critical.here.remote.clients.AutocompleteClient
import okhttp3.Interceptor
import org.junit.Assert.assertNotNull
import org.junit.Test

class ClientTest {


    @Test
    fun `test client initialization`(){
        val client = AutocompleteClient(Config.LogType.FULL, 3000, Interceptor { chain ->
            var request = chain.request()
            chain.proceed(request)
        })

        assertNotNull(client.getService())
    }

}