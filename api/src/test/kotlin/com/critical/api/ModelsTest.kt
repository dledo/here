package com.critical.api

import com.critical.here.remote.models.Address
import com.critical.here.remote.models.autocomplete.Suggestion
import com.critical.here.remote.models.autocomplete.Suggestions
import org.junit.Assert
import org.junit.Test


class ModelsTest {

    @Test
    fun `parse valid address`() {
        val address = TestUtils().getMockObject<Address>(this, "models/address.json", Address::class.java)
        Assert.assertNotNull(address)
        Assert.assertEquals("Deutschland", address?.country)
        Assert.assertEquals("Berlin", address?.state)
        Assert.assertEquals("Berlin", address?.county)
        Assert.assertEquals("Berlin", address?.city)
        Assert.assertEquals("Mitte", address?.district)
        Assert.assertEquals("Pariser Platz", address?.street)
        Assert.assertEquals("1", address?.houseNumber)
        Assert.assertEquals("3", address?.unit)
        Assert.assertEquals("10117", address?.postalCode)
    }

    @Test
    fun `parse valid suggestion` (){
        val suggestion = TestUtils().getMockObject<Suggestion>(this, "models/suggestion.json", Suggestion::class.java)
        Assert.assertNotNull(suggestion)
        Assert.assertEquals("Portugal, Vila Franca de Xira, Bom Sucesso", suggestion?.label)
        Assert.assertEquals("pt", suggestion?.language)
        Assert.assertEquals("PRT", suggestion?.countryCode)
        Assert.assertEquals("NT_F35bePGt2IY8FVFSmzB1.B", suggestion?.locationId)
        Assert.assertNotNull(suggestion?.address)
        Assert.assertEquals(666, suggestion?.distance)
        Assert.assertEquals("district", suggestion?.matchLevel)
    }

    @Test
    fun `parse valid suggestions` (){
        val suggestions = TestUtils().getMockObject<Suggestions>(this, "models/suggestions.json", Suggestions::class.java)
        Assert.assertNotNull(suggestions)
        Assert.assertNotNull(suggestions?.items)
        Assert.assertEquals(5, suggestions?.items?.size)
    }
}