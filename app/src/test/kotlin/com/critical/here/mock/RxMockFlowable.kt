package com.critical.here.mock


import io.reactivex.Flowable
import io.reactivex.exceptions.Exceptions

/**
 * Mock observable to simulate success calls in APIs
 */
class RxMockFlowable<T : Any> {

    fun success(result: T): Flowable<T> {
        return Flowable.just(result)
    }

    fun fail(throwable: Throwable?): Flowable<T> {
        if (throwable != null) {
            Exceptions.throwIfFatal(throwable)
            return Flowable.error(throwable)
        }
        return Flowable.error(NullPointerException())
    }
}
