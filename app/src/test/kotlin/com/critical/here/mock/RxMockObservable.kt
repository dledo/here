package com.critical.here.mock


import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions

/**
 * Mock observable to simulate success calls in APIs
 */
class RxMockObservable<T : Any> {

    fun success(result: T): Observable<T> {
        return Observable.just(result)
    }

    fun fail(throwable: Throwable?): Observable<T> {
        if (throwable != null) {
            Exceptions.throwIfFatal(throwable)
            return Observable.error(throwable)
        }
        return Observable.error(NullPointerException())
    }

}
