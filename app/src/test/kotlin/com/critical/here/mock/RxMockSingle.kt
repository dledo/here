package com.critical.here.mock


import io.reactivex.Single
import io.reactivex.exceptions.Exceptions

/**
 * Mock observable to simulate success calls in APIs
 */
class RxMockSingle<T : Any> {

    fun success(result: T): Single<T> {
        return Single.just(result)
    }

    fun fail(throwable: Throwable?): Single<T> {
        if (throwable != null) {
            Exceptions.throwIfFatal(throwable)
            return Single.error(throwable)
        }
        return Single.error(NullPointerException())
    }
}
