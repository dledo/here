package com.critical.here.detail

import com.critical.here.BaseViewModelTest
import com.critical.here.TestUtils
import com.critical.here.mock.RxMockFlowable
import com.critical.here.mock.RxMockSingle
import com.critical.here.remote.geocoder.DisplayPosition
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class DetailVMTest : BaseViewModelTest<DetailVM>() {


    @Before
    override fun testSetup() {
        super.testSetup()
        viewModel = DetailVM(mHereApi, mFavoritesRep)

    }

    @Test
    fun `load display position`() {
        val position = TestUtils().getMockObject<DisplayPosition>(this, "models/displayPosition.json", DisplayPosition::class.java)

        Mockito.`when`(mHereApi.loadGeoPosition(ArgumentMatchers.anyString()))
                .thenReturn(RxMockSingle<DisplayPosition>().success(position!!))

        val values = viewModel.loadDisplayPosition(ArgumentMatchers.anyString())
                .test()
                .assertNoErrors()
                .values()

        assertNotNull(values)
        assertEquals(41.16499, values[0].latitude)
        assertEquals(-8.63945, values[0].longitude)
    }

    @Test
    fun `is a favorite`() {
        Mockito.`when`(mFavoritesRep.isFavorite(ArgumentMatchers.anyString()))
                .thenReturn(RxMockFlowable<Int>().success(1))

        val values = viewModel.isFavorite(ArgumentMatchers.anyString())
                .test()
                .assertNoErrors()
                .values()

        assertNotNull(values)
        assertEquals(1, values[0])
    }

}