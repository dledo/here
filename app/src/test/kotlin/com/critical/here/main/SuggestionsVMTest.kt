package com.critical.here.main

import com.critical.here.BaseViewModelTest
import com.critical.here.TestUtils
import com.critical.here.main.suggestions.SuggestionsVM
import com.critical.here.mock.RxMockSingle
import com.critical.here.remote.models.autocomplete.Suggestions
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class SuggestionsVMTest : BaseViewModelTest<SuggestionsVM>() {


    @Before
    override fun testSetup() {
        super.testSetup()
        viewModel = SuggestionsVM(mHereApi)

    }

    @Test
    fun `suggestion empty search`() {

        val values =viewModel.searchSuggestions("", locationMock, 0)
                .test()
                .assertNoErrors()
                .assertValueCount(1)
                .values()
        assertEquals(0,values[0].size)
    }

    @Test
    fun `suggestion search with distance sort`() {

        val suggestions = TestUtils().getMockObject<Suggestions>(this, "models/suggestions.json", Suggestions::class.java)

        `when`(mHereApi.searchSuggestions("porto", null))
                .thenReturn(RxMockSingle<Suggestions>().success(suggestions!!))

        val values = viewModel.searchSuggestions("porto", locationMock,0)
                .test()
                .assertNoErrors()
                .assertValueCount(1)
                .values()

        assertNotNull(values)
        assertEquals(5,values[0].size)
        assertEquals("NT_xXibCE13xjz.HMdYP5miYB", values[0][0].locationId)
    }

    @Test
    fun `suggestion search with name sort`() {

        val suggestions = TestUtils().getMockObject<Suggestions>(this, "models/suggestions.json", Suggestions::class.java)

        `when`(mHereApi.searchSuggestions("porto", null))
                .thenReturn(RxMockSingle<Suggestions>().success(suggestions!!))

        val values = viewModel.searchSuggestions("porto",locationMock, 1)
                .test()
                .assertNoErrors()
                .values()

        assertNotNull(values)
        assertEquals(5,values[0].size)
        assertEquals("NT_awMja08ldnoS9wpQpZAGJB", values[0][0].locationId)
    }

}