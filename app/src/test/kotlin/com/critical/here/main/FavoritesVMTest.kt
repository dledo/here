package com.critical.here.main

import com.critical.here.BaseViewModelTest
import com.critical.here.main.favorites.FavoritesVM
import com.critical.here.mock.RxMockFlowable
import com.critical.here.persistence.SuggestionEntity
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class FavoritesVMTest : BaseViewModelTest<FavoritesVM>() {


    @Before
    override fun testSetup() {
        super.testSetup()
        viewModel = FavoritesVM(mFavoritesRep)


    }

    @Test
    fun `get favorites`() {
        Mockito.`when`(mFavoritesRep.loadFavSuggestions())
                .thenReturn(RxMockFlowable<List<SuggestionEntity>>().success(ArrayList()))
        val values = viewModel.favSuggestions
                .test()
                .assertNoErrors()
                .values()

        assertNotNull(values)
        assertEquals(0, values[0].size)
    }


}