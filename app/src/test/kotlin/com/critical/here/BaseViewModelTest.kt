package com.critical.here

import android.arch.lifecycle.ViewModel
import android.content.Context
import android.location.Location
import android.support.annotation.CallSuper
import com.critical.here.remote.HereApi
import com.critical.here.repository.FavoritesRep
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner


/**
 * Base test for all presenters
 */

@RunWith(PowerMockRunner::class)
@PrepareForTest(FavoritesRep::class, HereApi::class)
abstract class BaseViewModelTest<VM : ViewModel> {

    lateinit var viewModel: VM

    @Mock
    protected lateinit var mHereApi: HereApi

    @Mock
    protected lateinit var mFavoritesRep: FavoritesRep

    // create mock context
    var contextMock: Context = Mockito.mock(Context::class.java)
    var locationMock: Location = Mockito.mock(Location::class.java)


    init {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @CallSuper
    @Before
    open fun testSetup() {
        MockitoAnnotations.initMocks(this)
        // Here api
        PowerMockito.mockStatic(HereApi::class.java)
        // Fav repository
        PowerMockito.mockStatic(FavoritesRep::class.java)

    }

    fun makeLocation(latitude: Double, longitude: Double): Location {
        val location = Location("gps")
        location.latitude = latitude
        location.longitude = longitude
        location.accuracy = 100.0f
        location.time = System.currentTimeMillis()
        return location
    }

}
