package com.critical.here.main.favorites;

import com.critical.here.R;
import com.critical.here.persistence.SuggestionEntity;
import com.critical.here.utils.StringUtils;
import com.critical.here.views.adapters.BaseAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FavoritesAdapter extends BaseAdapter<SuggestionEntity, FavoritesAdapter.SuggestionVH> {

    static class SuggestionVH extends RecyclerView.ViewHolder {

        final TextView cellLabel;
        final TextView cellMatch;
        final TextView cellCity;
        final TextView cellCounty;
        final TextView cellCountry;
        final TextView cellDistance;


        SuggestionVH(final View itemView) {
            super(itemView);
            cellLabel = itemView.findViewById(R.id.suggestion_label);
            cellMatch = itemView.findViewById(R.id.suggestion_match_level);
            cellCity = itemView.findViewById(R.id.suggestion_city);
            cellCounty = itemView.findViewById(R.id.suggestion_county);
            cellCountry = itemView.findViewById(R.id.suggestion_country);
            cellDistance = itemView.findViewById(R.id.suggestion_distance);
        }
    }


    @Override
    protected void viewHolderBinding(final SuggestionVH holder, final int position) {
        final SuggestionEntity suggestion = getItem(position);
        if (suggestion != null && holder != null) {
            holder.cellLabel.setText(suggestion.getLabel());
            holder.cellMatch.setText(suggestion.getMatchLevel());

            if (suggestion.getCity() != null) {
                holder.cellCity.setText(suggestion.getCity());
                holder.cellCity.setVisibility(View.VISIBLE);
            } else {
                holder.cellCity.setVisibility(View.GONE);
            }
            if (suggestion.getCounty() != null) {
                holder.cellCounty.setText(suggestion.getCounty());
                holder.cellCounty.setVisibility(View.VISIBLE);
            } else if (suggestion.getState() != null) {
                holder.cellCounty.setText(suggestion.getState());
                holder.cellCounty.setVisibility(View.VISIBLE);
            } else {
                holder.cellCounty.setVisibility(View.GONE);
            }
            if (suggestion.getCountry() != null) {
                holder.cellCountry.setText(suggestion.getCountry());
                holder.cellCountry.setVisibility(View.VISIBLE);
            } else {
                holder.cellCountry.setVisibility(View.GONE);
            }

            if (suggestion.getDistance() != 0) {
                holder.cellDistance.setText(StringUtils.distanceKm(suggestion.getDistance()));
                holder.cellDistance.setVisibility(View.VISIBLE);
            } else {
                holder.cellDistance.setVisibility(View.GONE);
            }
        }
    }

    @NonNull
    @Override
    public SuggestionVH onCreateViewHolder(@NonNull final ViewGroup parent, final int i) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_suggestion, parent, false);
        return new FavoritesAdapter.SuggestionVH(view);
    }
}
