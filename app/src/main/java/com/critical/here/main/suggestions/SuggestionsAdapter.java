package com.critical.here.main.suggestions;

import com.critical.here.R;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.utils.StringUtils;
import com.critical.here.views.adapters.BaseAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SuggestionsAdapter extends BaseAdapter<Suggestion, SuggestionsAdapter.SuggestionVH> {

    static class SuggestionVH extends RecyclerView.ViewHolder {

        final TextView cellLabel;
        final TextView cellMatch;
        final TextView cellCity;
        final TextView cellCounty;
        final TextView cellCountry;
        final TextView cellDistance;


        SuggestionVH(final View itemView) {
            super(itemView);
            cellLabel = itemView.findViewById(R.id.suggestion_label);
            cellMatch = itemView.findViewById(R.id.suggestion_match_level);
            cellCity = itemView.findViewById(R.id.suggestion_city);
            cellCounty = itemView.findViewById(R.id.suggestion_county);
            cellCountry = itemView.findViewById(R.id.suggestion_country);
            cellDistance = itemView.findViewById(R.id.suggestion_distance);
        }
    }


    @Override
    protected void viewHolderBinding(final SuggestionVH holder, final int position) {
        final Suggestion suggestion = getItem(position);
        if (suggestion != null && holder != null) {
            holder.cellLabel.setText(suggestion.getLabel());
            holder.cellMatch.setText(suggestion.getMatchLevel());

            if (suggestion.getAddress() != null) {
                if (suggestion.getAddress().getCity() != null) {
                    holder.cellCity.setText(suggestion.getAddress().getCity());
                    holder.cellCity.setVisibility(View.VISIBLE);
                }else {
                    holder.cellCity.setVisibility(View.GONE);
                }
                if (suggestion.getAddress().getCounty() != null) {
                    holder.cellCounty.setText(suggestion.getAddress().getCounty());
                    holder.cellCounty.setVisibility(View.VISIBLE);
                } else if (suggestion.getAddress().getState() != null) {
                    holder.cellCounty.setText(suggestion.getAddress().getState());
                    holder.cellCounty.setVisibility(View.VISIBLE);
                } else {
                    holder.cellCounty.setVisibility(View.GONE);
                }
                if (suggestion.getAddress().getCountry() != null) {
                    holder.cellCountry.setText(suggestion.getAddress().getCountry());
                    holder.cellCountry.setVisibility(View.VISIBLE);
                }else {
                    holder.cellCountry.setVisibility(View.GONE);
                }
            }
            if (suggestion.getDistance() != 0) {
                holder.cellDistance.setText(StringUtils.distanceKm(suggestion.getDistance()));
                holder.cellDistance.setVisibility(View.VISIBLE);
            } else {
                holder.cellDistance.setVisibility(View.GONE);
            }
        }
    }

    @NonNull
    @Override
    public SuggestionVH onCreateViewHolder(@NonNull final ViewGroup parent, final int i) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_suggestion, parent, false);
        return new SuggestionsAdapter.SuggestionVH(view);
    }
}
