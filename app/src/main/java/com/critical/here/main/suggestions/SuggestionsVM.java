package com.critical.here.main.suggestions;

import com.critical.here.remote.HereApi;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.remote.models.autocomplete.Suggestions;
import com.critical.here.utils.Comparators;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.location.Location;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class SuggestionsVM extends ViewModel {

    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final HereApi api;

        Factory(final HereApi api) {
            this.api = api;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
            return (T) new SuggestionsVM(api);
        }
    }

    private final HereApi mHereApi;

    public SuggestionsVM(final HereApi api) {
        mHereApi = api;
    }


    @SuppressLint("MissingPermission")
    public Observable<List<Suggestion>> searchSuggestions(final String query, final Location location, final int sortIndex) {

        if (query == null || query.length() == 0) {
            return Observable.just(new ArrayList<>());
        }

        if (location != null && location.getLatitude() != 0 && location.getLongitude() != 0) {
            return mHereApi.searchSuggestions(query,
                    String.format(Locale.US, "%f,%f", location.getLatitude(), location.getLongitude()))
                    .flatMapObservable((Function<Suggestions, ObservableSource<List<Suggestion>>>) suggestions -> SuggestionsVM.this.sort(sortIndex, suggestions.getItems()))
                    .onErrorReturnItem(new ArrayList<>());
        } else {
            return mHereApi.searchSuggestions(query, null)
                    .flatMapObservable((Function<Suggestions, ObservableSource<List<Suggestion>>>)
                            suggestions -> SuggestionsVM.this.sort(sortIndex, suggestions.getItems()))
                    .onErrorReturnItem(new ArrayList<>());
        }
    }


    public Observable<List<Suggestion>> sort(final int indexCriteria, final List<Suggestion> data) {
        if (data != null) {
            return Observable.fromCallable(() -> {
                final ArrayList<Suggestion> sortedList = new ArrayList<>(data);
                if (indexCriteria == 0) {
                    Collections.sort(sortedList, new Comparators.SuggestionDistanceComparator());
                } else if (indexCriteria == 1) {
                    Collections.sort(sortedList, new Comparators.SuggestionNameComparator());
                }
                return sortedList;
            });
        }
        return Observable.just(new ArrayList<>());
    }
}
