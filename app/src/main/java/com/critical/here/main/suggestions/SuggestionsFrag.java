package com.critical.here.main.suggestions;

import com.critical.here.BaseFragment;
import com.critical.here.R;
import com.critical.here.detail.DetailActivity;
import com.critical.here.main.MainActivity;
import com.critical.here.main.favorites.FavoritesFrag;
import com.critical.here.remote.HereApi;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.utils.rx.RxResult;
import com.critical.here.utils.rx.SearchQueryObservable;
import com.critical.here.views.VerticalSpaceDecoration;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class SuggestionsFrag extends BaseFragment<SuggestionsVM> {

    private static final int DEBOUNCE_TIME = 300;
    private static final String LAST_QUERY = "QUERY";
    public static final String TAG = SuggestionsFrag.class.getSimpleName();

    public static SuggestionsFrag newInstance() {
        return new SuggestionsFrag();
    }

    private AppCompatSpinner mFilterSpinner;
    private SearchView mSearchView;
    private RecyclerView mSuggestionsRV;
    private SuggestionsAdapter mAdapter;
    private FloatingActionButton mFavoritesFab;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_suggestions, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSearchView = view.findViewById(R.id.suggestions_search_view);
        addDisposable(
                SearchQueryObservable.queryChanges(mSearchView)
                        .debounce(DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(charSequence -> makeASearch(charSequence.toString()))

        );

        mFilterSpinner = view.findViewById(R.id.filter_spinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.suggestions_filter_options, R.layout.filter_item);
        adapter.setDropDownViewResource(R.layout.filter_drop_down_item);
        mFilterSpinner.setAdapter(adapter);
        mFilterSpinner.setSelection(0);
        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (mViewModel != null && mAdapter != null) {
                    addDisposable(
                            mViewModel.sort(mFilterSpinner.getSelectedItemPosition(), mAdapter.getAllItems())
                                    .compose(applyTransformationAndBind())
                                    .startWith(RxResult.loading())
                                    .subscribe(rxResult -> {
                                        showLoading(rxResult.status == RxResult.Status.LOADING);
                                        switch (rxResult.status) {
                                            case SUCCESS:
                                                if (mAdapter != null) {
                                                    mAdapter.addAll(rxResult.data);
                                                }
                                                break;
                                        }
                                    })
                    );
                }
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });


        mSuggestionsRV = view.findViewById(R.id.suggestions_recycler_view);
        mSuggestionsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        mSuggestionsRV.addItemDecoration(new VerticalSpaceDecoration(getResources().getDimensionPixelSize(R.dimen.list_cell_spacer)));
        mSuggestionsRV.setOnTouchListener((v, event) -> {
            showKeyBoard(false);
            return false;
        });
        mAdapter = new SuggestionsAdapter();
        mAdapter.setRecyclerItemClickListener((position, viewHolder) -> {
            if (getActivity() != null) {
                final Suggestion suggestion = mAdapter.getItem(position);
                final Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra(DetailActivity.SUGGESTION_KEY, suggestion);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
        mSuggestionsRV.setAdapter(mAdapter);

        mFavoritesFab = view.findViewById(R.id.generate_refs_fab);
        mFavoritesFab.setOnClickListener(v -> {
            if (getActivity() != null) {
                addFragment(FavoritesFrag.newInstance(), FavoritesFrag.TAG);
            }
        });

        // restore
        if (savedInstanceState != null) {
            if (savedInstanceState.get(LAST_QUERY) != null) {
                makeASearch(savedInstanceState.getString(LAST_QUERY));
            }
        }
    }

    @Override
    public ViewModelProvider.NewInstanceFactory getViewModelFactory() {
        return new SuggestionsVM.Factory(HereApi.getInstance());
    }


    @SuppressLint("CheckResult")
    private void makeASearch(final String query) {
        addDisposable(
                mViewModel.searchSuggestions(query, ((MainActivity) getActivity()).getCurrentLocation(),
                        mFilterSpinner.getSelectedItemPosition())
                        .compose(applyTransformationAndBind())
                        .startWith(RxResult.loading())
                        .subscribe(rxResult -> {
                            showLoading(rxResult.status == RxResult.Status.LOADING);
                            switch (rxResult.status) {
                                case SUCCESS:
                                    if (mAdapter != null) {
                                        mAdapter.addAll(rxResult.data);
                                    }
                                    break;
                                case ERROR:
                                    //TODO
                                    break;
                            }
                        })
        );
    }

}
