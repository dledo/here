package com.critical.here.main;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import com.critical.here.BaseActivity;
import com.critical.here.R;
import com.critical.here.main.suggestions.SuggestionsFrag;
import com.critical.here.utils.FragOperation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

public class MainActivity extends BaseActivity {

    private static final long UPDATE_INTERVAL = 60000; // 1 minute
    private static final int LOCATION_PERMISSIONS_CODE = 1;

    private static final String[] LOC_PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    private FusedLocationProviderClient mLocationClient;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            executeFragOperation(new FragOperation(FragOperation.OP.REPLACE, SuggestionsFrag.newInstance(), SuggestionsFrag.TAG));
        }

        mLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(final LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
            }
        };
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!isPermissionsGranted(LOC_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, LOC_PERMISSIONS, LOCATION_PERMISSIONS_CODE);
        } else {
            startReadLocation();
        }
    }


    @Override
    protected void onStop() {
        if (!isPermissionsGranted(LOC_PERMISSIONS)) {
            stopReadLocation();
        }
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (isPermissionsGranted(permissions)) {
            startReadLocation();
        }
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    @SuppressLint("MissingPermission")
    private void startReadLocation() {
        if (mLocationClient != null && mLocationCallback != null) {
            final LocationRequest request = new LocationRequest();
            request.setInterval(UPDATE_INTERVAL);
            request.setFastestInterval(UPDATE_INTERVAL / 2);
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationClient.requestLocationUpdates(request,
                    mLocationCallback, Looper.myLooper());
        }
    }

    private void stopReadLocation() {
        if (mLocationClient != null && mLocationCallback != null) {
            mLocationClient.removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this, task -> {
                        //...
                    });
        }
    }
}
