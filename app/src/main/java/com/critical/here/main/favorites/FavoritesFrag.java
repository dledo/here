package com.critical.here.main.favorites;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.critical.here.BaseFragment;
import com.critical.here.R;
import com.critical.here.persistence.SuggestionEntity;
import com.critical.here.repository.FavoritesRep;
import com.critical.here.views.VerticalSpaceDecoration;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FavoritesFrag extends BaseFragment<FavoritesVM> implements OnMapReadyCallback {

    public static final String TAG = FavoritesFrag.class.getSimpleName();
    private static final String LAST_LAT = "LAT";
    private static final String LAST_LON = "LON";

    private GoogleMap mFavMap;
    private FavoritesAdapter mAdapter;

    private double mLastLatitude = 0;
    private double mLastLongitude = 0;


    public static FavoritesFrag newInstance() {
        return new FavoritesFrag();
    }

    @Override
    protected ViewModelProvider.NewInstanceFactory getViewModelFactory() {
        return new FavoritesVM.Factory(FavoritesRep.getInstance(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_favorites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            mLastLatitude = savedInstanceState.getDouble(LAST_LAT);
            mLastLongitude = savedInstanceState.getDouble(LAST_LON);
        }

        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.fav_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        final RecyclerView favoritesRV = view.findViewById(R.id.favorites_recycler_view);
        favoritesRV.setLayoutManager(new LinearLayoutManager(getContext()));
        favoritesRV.addItemDecoration(new VerticalSpaceDecoration(getResources().getDimensionPixelSize(R.dimen.list_cell_spacer)));

        mAdapter = new FavoritesAdapter();
        mAdapter.setRecyclerItemClickListener((position, viewHolder) -> {
            if (getActivity() != null) {
                final SuggestionEntity suggestion = mAdapter.getItem(position);
                if (suggestion != null && suggestion.getLatitude() != 0 && suggestion.getLongitude() != 0) {
                    setMapPoint(suggestion.getLatitude(), suggestion.getLongitude());
                }
            }
        });
        favoritesRV.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showLoading(true);
        addDisposable(
                mViewModel.getFavSuggestions()
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(suggestionEntities -> {
                            if (mAdapter != null) {
                                mAdapter.addAll(suggestionEntities);
                            }
                            showLoading(false);
                        }, e -> System.out.println("getFavSuggestions: " + e.getMessage()))
        );

    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        outState.putDouble(LAST_LAT, mLastLatitude);
        outState.putDouble(LAST_LON, mLastLongitude);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mFavMap = googleMap;
        setMapPoint(mLastLatitude, mLastLongitude);
    }

    private void setMapPoint(final double latitude, final double longitude) {
        if (mFavMap != null && latitude != 0 && longitude != 0) {
            mLastLatitude = latitude;
            mLastLongitude = longitude;
            final LatLng latLng = new LatLng(latitude, longitude);
            mFavMap.clear();
            mFavMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .alpha(0.8f));
            final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            mFavMap.animateCamera(cameraUpdate);
        }
    }
}
