package com.critical.here.main.favorites;

import com.critical.here.persistence.SuggestionEntity;
import com.critical.here.repository.FavoritesRep;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Flowable;

public class FavoritesVM extends ViewModel {


    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final FavoritesRep rep;

        Factory(final FavoritesRep rep) {
            this.rep = rep;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
            return (T) new FavoritesVM(rep);
        }
    }

    private final FavoritesRep mFavoritesRep;

    public FavoritesVM(final FavoritesRep rep) {
        mFavoritesRep = rep;
    }


    public Flowable<List<SuggestionEntity>> getFavSuggestions() {
        return mFavoritesRep.loadFavSuggestions();
    }

}
