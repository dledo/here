package com.critical.here.detail;

import com.critical.here.BaseFragment;
import com.critical.here.R;
import com.critical.here.remote.HereApi;
import com.critical.here.remote.geocoder.DisplayPosition;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.repository.FavoritesRep;
import com.critical.here.utils.StringUtils;
import com.critical.here.utils.rx.RxResult;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DetailFrag extends BaseFragment<DetailVM> {

    public static final String TAG = DetailFrag.class.getSimpleName();
    private static final String SUGGESTION_KEY = "SUGGESTION";

    public interface DetailFragListener {
        void onDisplayPositionLoaded(DisplayPosition displayPosition);
    }

    private DetailFragListener mCallback;

    private TextView mStreet;
    private TextView mPostalCode;
    private TextView mCoordinates;
    private TextView mDistance;
    private Button mFavorite;


    public static DetailFrag newInstance(final Suggestion suggestion) {
        final DetailFrag frag = new DetailFrag();
        final Bundle b = new Bundle();
        b.putSerializable(SUGGESTION_KEY, suggestion);
        frag.setArguments(b);
        return frag;
    }

    @Override
    protected ViewModelProvider.NewInstanceFactory getViewModelFactory() {
        return new DetailVM.Factory(HereApi.getInstance(), FavoritesRep.getInstance(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStreet = view.findViewById(R.id.suggestion_street);
        mPostalCode = view.findViewById(R.id.suggestion_postal_code);
        mCoordinates = view.findViewById(R.id.suggestion_coordinates);
        mDistance = view.findViewById(R.id.suggestion_distance);
        mFavorite = view.findViewById(R.id.add_to_favorites);
        mFavorite.setOnClickListener(v -> mViewModel.switchFavorite(getArgumentSuggestion()));
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Suggestion suggestion = getArgumentSuggestion();

        addDisposable(
                mViewModel.loadDisplayPosition(suggestion.getLocationId())
                        .compose(applyTransformationAndBind())
                        .startWith(RxResult.loading())
                        .subscribe(rxResult -> {
                            showLoading(rxResult.status == RxResult.Status.LOADING);
                            switch (rxResult.status) {
                                case SUCCESS:
                                    if (mCallback != null) {
                                        mCallback.onDisplayPositionLoaded(rxResult.data);
                                        mCoordinates.setText(String.format(Locale.US, "Lat:%f, Long:%f",
                                                rxResult.data.getLatitude(),
                                                rxResult.data.getLongitude()));
                                        mFavorite.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case ERROR:
                                    // nothing to do
                                    break;

                            }
                        })
        );

        addDisposable(
                mViewModel.isFavorite(suggestion.getLocationId())
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<Integer>() {
                            @Override
                            public void accept(final Integer count) throws Exception {
                                mFavorite.setText(count == 0 ? R.string.add_favorites : R.string.remove_favorites);
                            }
                        }, e -> System.out.println("isFavorite: " + e.getMessage()))
        );

        if (suggestion.getAddress() != null) {
            mStreet.setText(suggestion.getAddress().getStreet());
            mPostalCode.setText(suggestion.getAddress().getPostalCode());
        }
        if (suggestion.getDistance() != 0) {
            mDistance.setText(StringUtils.distanceKm(suggestion.getDistance()));
        }

    }

    public void setDetailFragListener(final DetailFragListener callback) {
        this.mCallback = callback;
    }

    private Suggestion getArgumentSuggestion() {
        return (Suggestion) getArguments().getSerializable(DetailFrag.SUGGESTION_KEY);
    }


}
