package com.critical.here.detail;

import com.critical.here.remote.HereApi;
import com.critical.here.remote.geocoder.DisplayPosition;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.repository.FavoritesRep;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class DetailVM extends ViewModel {


    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final HereApi api;
        private final FavoritesRep rep;

        Factory(final HereApi api, final FavoritesRep rep) {
            this.api = api;
            this.rep = rep;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
            return (T) new DetailVM(api, rep);
        }
    }

    private final HereApi mHereApi;
    private final FavoritesRep mFavRep;

    public DetailVM(final HereApi api, final FavoritesRep rep) {
        mHereApi = api;
        mFavRep = rep;
    }

    private DisplayPosition mDisplayPosition;
    private int mOnFavCount = 0;

    public Observable<DisplayPosition> loadDisplayPosition(final String id) {
        return mHereApi.loadGeoPosition(id)
                .doOnSuccess(displayPosition -> mDisplayPosition = displayPosition).toObservable();
    }

    public Flowable<Integer> isFavorite(final String id) {
        return mFavRep.isFavorite(id)
                .doOnNext(integer -> mOnFavCount = integer);
    }

    public void switchFavorite(final Suggestion suggestion) {
        mFavRep.addFavSuggestion(suggestion, mDisplayPosition, mOnFavCount == 0);
    }

}
