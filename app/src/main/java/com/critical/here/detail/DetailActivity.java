package com.critical.here.detail;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.critical.here.BaseActivity;
import com.critical.here.R;
import com.critical.here.remote.geocoder.DisplayPosition;
import com.critical.here.remote.models.autocomplete.Suggestion;
import com.critical.here.utils.FragOperation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public class DetailActivity extends BaseActivity implements OnMapReadyCallback,
        DetailFrag.DetailFragListener {

    public static final String SUGGESTION_KEY = "SuggestionEntity";

    private DisplayPosition mCurrentdDisplayPosition;
    private GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        if (savedInstanceState == null) {
            executeFragOperation(new FragOperation(FragOperation.OP.REPLACE,
                    DetailFrag.newInstance((Suggestion) getIntent().getSerializableExtra(SUGGESTION_KEY)),
                    DetailFrag.TAG));
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onAttachFragment(final Fragment fragment) {
        if (fragment instanceof DetailFrag) {
            final DetailFrag frag = (DetailFrag) fragment;
            frag.setDetailFragListener(this);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.pop_slide_in, R.anim.pop_slide_out);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        setMapPoint(mCurrentdDisplayPosition);
    }


    @Override
    public void onDisplayPositionLoaded(final DisplayPosition displayPosition) {
        mCurrentdDisplayPosition = displayPosition;
        setMapPoint(displayPosition);
    }

    private void setMapPoint(final DisplayPosition displayPosition) {
        if (mMap != null && displayPosition != null) {
            final LatLng latLng = new LatLng(displayPosition.getLatitude(), displayPosition.getLongitude());
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .alpha(0.8f));
            final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            mMap.animateCamera(cameraUpdate);
        }
    }
}
