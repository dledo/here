package com.critical.here;

import com.critical.here.remote.Config;
import com.critical.here.remote.HereApi;

import android.app.Application;

public class Here extends Application {

    final long TIMEOUT = 30000;

    @Override
    public void onCreate() {
        super.onCreate();

        final Config config = new Config(BuildConfig.HERE_APP_ID,BuildConfig.HERE_APP_CODE,TIMEOUT);
        config.setMLogType(Config.LogType.FULL);
        HereApi.create(config);

    }
}
