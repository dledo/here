package com.critical.here.utils.rx;

import android.support.v7.widget.SearchView;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.MainThreadDisposable;

/**
 * Observable that emits items when afterTextChange from SearchView is triggered
 */

public class SearchQueryObservable extends Observable<CharSequence> {

    final static class Listener extends MainThreadDisposable implements SearchView.OnQueryTextListener {
        private final SearchView searchView;
        private final Observer<? super CharSequence> observer;

        Listener(final SearchView view, final Observer<? super CharSequence> observer) {
            this.searchView = view;
            this.observer = observer;
        }


        @Override
        protected void onDispose() {
            searchView.setOnQueryTextListener(null);
        }

        @Override
        public boolean onQueryTextSubmit(final String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(final String s) {
            if (!isDisposed()) {
                observer.onNext(s);
            }
            return true;
        }
    }
    private final SearchView view;

    private SearchQueryObservable(final SearchView searchView) {
        this.view = searchView;
    }

    public static Observable<CharSequence> queryChanges
            (final SearchView searchView) {
        return new SearchQueryObservable(searchView);
    }

    @Override
    protected void subscribeActual(final Observer<? super CharSequence> observer) {
        final Listener listener = new Listener(view, observer);
        observer.onSubscribe(listener);
        view.setOnQueryTextListener(listener);
    }
}
