package com.critical.here.utils;

import android.support.v4.app.Fragment;

public interface FragNavigation {

    void addFragment(final Fragment fragment, final String tag);

    void replaceFragment(final Fragment fragment, final String tag);

    void removeLastFragment();
}
