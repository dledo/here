package com.critical.here.utils;


import android.support.v4.app.Fragment;

public class FragOperation {

    public enum OP {
        ADD, REPLACE, POP
    }

    public Fragment fragment;
    public String tag;
    public OP op;
    public int enterAnim = 0;
    public int exitAnim = 0;

    public FragOperation(final OP op) {
        if (op != OP.POP) {
            throw new IllegalArgumentException("Only POP operation is supported for this constructor");
        }
        this.op = op;
    }

    public FragOperation(final OP operation, final Fragment frag, final String tag) {
        this.op = operation;
        this.fragment = frag;
        this.tag = tag;
    }

}
