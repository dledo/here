package com.critical.here.utils.rx;

import com.critical.here.remote.models.RequestError;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


/**
 * This class represents the state of the UI when a call is made.
 * It has one of 3 states: SUCCESS, ERROR, LOADING
 */

public class RxResult<T> {


    /**
     * UI STATES
     */
    public enum Status {
        SUCCESS, ERROR, LOADING
    }

    @NonNull
    public final Status status;
    @Nullable
    public final RequestError requestError;


    @Nullable
    public final T data;

    private RxResult(@NonNull final Status status, @Nullable final T data, @Nullable final RequestError error) {
        this.status = status;
        this.data = data;
        this.requestError = error;
    }

    public static <T> RxResult<T> success(@Nullable final T data) {
        return new RxResult<>(Status.SUCCESS, data, null);
    }

    public static <T> RxResult<T> error(final RequestError error) {
        return new RxResult<>(Status.ERROR, null, error);
    }

    public static <T> RxResult<T> loading() {
        return new RxResult<>(Status.LOADING, null, null);
    }
}
