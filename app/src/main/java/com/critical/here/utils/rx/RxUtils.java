package com.critical.here.utils.rx;

import com.critical.here.remote.models.RequestError;

import io.reactivex.ObservableTransformer;
import io.reactivex.functions.Function;

/**
 * Helper class to implement RxJava
 */

public class RxUtils {

    /**
     * Observable transformer that encapsulates the date in a RxResult which contains the state of
     * said object.
     */
    public static <T> ObservableTransformer<T, RxResult<T>> transformToRxResult() {
        return upstream -> upstream
                .map(RxResult::success)
                .onErrorReturn((Function<Throwable, RxResult<T>>) throwable -> {
                    return RxResult.error(new RequestError(RequestError.Type.OTHER, throwable));
                });
    }
}
