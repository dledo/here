package com.critical.here.utils;

import com.critical.here.remote.models.autocomplete.Suggestion;

import java.util.Comparator;

public class Comparators {
    public static class SuggestionNameComparator implements Comparator<Suggestion> {
        @Override
        public int compare(final Suggestion left, final Suggestion right) {
            if (left != null && left.getLabel() != null && right != null && right.getLabel() != null) {
                return left.getLabel().compareTo(right.getLabel());
            }
            return 0;
        }
    }

    public static class SuggestionDistanceComparator implements Comparator<Suggestion> {
        @Override
        public int compare(final Suggestion left, final Suggestion right) {
            if (left != null && left.getDistance() != 0 && right != null && right.getDistance() != 0) {
                return right.getDistance() - left.getDistance();
            }
            return 0;
        }
    }
}
