package com.critical.here.utils;

import java.util.Locale;

public class StringUtils {

    public static String distanceKm(final int distanceMeters) {
        return String.format(Locale.US, "%.2f Km", distanceMeters * 0.001f);
    }
}
