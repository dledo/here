package com.critical.here;

import com.critical.here.utils.FragNavigation;
import com.critical.here.utils.FragOperation;
import com.critical.here.utils.rx.RxResult;
import com.critical.here.utils.rx.RxUtils;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import java.lang.reflect.ParameterizedType;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseFragment<VM extends ViewModel> extends
        Fragment implements FragNavigation {

    protected VM mViewModel;
    private ProgressBar mHorizontalProgress;
    private CompositeDisposable mCompositeDisposable;


    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // create view model
        try {
            final ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
            final Class<VM> type = (Class<VM>) superClass.getActualTypeArguments()[0];
            mViewModel = ViewModelProviders.of(this, getViewModelFactory()).get(type);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHorizontalProgress = view.findViewById(R.id.here_horizontal_progress);
    }

    @Override
    public void onDestroyView() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
        super.onDestroyView();
    }

    protected void addDisposable(final Disposable d) {
        mCompositeDisposable.add(d);
    }

    @Override
    public void replaceFragment(final Fragment fragment, final String tag) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).executeFragOperation(new FragOperation(FragOperation.OP.REPLACE, fragment, tag));
        }
    }

    @Override
    public void addFragment(final Fragment fragment, final String tag) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            final FragOperation operation = new FragOperation(FragOperation.OP.ADD, fragment, tag);
            operation.enterAnim = R.anim.bottom_slide_in;
            operation.exitAnim = R.anim.bottom_slide_out;
            ((BaseActivity) getActivity()).executeFragOperation(operation);
        }
    }


    @Override
    public void removeLastFragment() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).executeFragOperation(new FragOperation(FragOperation.OP.POP));
        }
    }

    protected void showLoading(final boolean show) {
        if (mHorizontalProgress != null) {
            mHorizontalProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected void showKeyBoard(final boolean show) {
        final View view = getView();
        if (view != null && getActivity() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context
                    .INPUT_METHOD_SERVICE);
            if (imm != null) {
                if (show) {
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                } else {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    protected static <S> ObservableTransformer<S, RxResult<S>> applyTransformationAndBind() {
        return upstream -> upstream
                .compose(RxUtils.transformToRxResult())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected ViewModelProvider.NewInstanceFactory getViewModelFactory() {
        return new ViewModelProvider.NewInstanceFactory();
    }
}
