package com.critical.here.repository;

import com.critical.here.persistence.HereDataBase;
import com.critical.here.persistence.SuggestionEntity;
import com.critical.here.remote.geocoder.DisplayPosition;
import com.critical.here.remote.models.autocomplete.Suggestion;

import android.content.Context;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class FavoritesRep {


    private static volatile FavoritesRep sInstance;

    private final HereDataBase mDatabase;

    private FavoritesRep(final HereDataBase database) {
        mDatabase = database;
    }

    public static FavoritesRep getInstance(final Context context) {
        FavoritesRep result = sInstance;
        if (result == null) {
            synchronized (FavoritesRep.class) {
                result = sInstance;
                if (result == null) {
                    sInstance = result = new FavoritesRep(HereDataBase.get(context));
                }
            }
        }
        return result;
    }


    public Flowable<List<SuggestionEntity>> loadFavSuggestions() {
        return mDatabase.suggestionDao().getAllSuggestions();
    }

    public Flowable<Integer> isFavorite(final String id) {
        return mDatabase.suggestionDao().count(id);
    }


    public void addFavSuggestion(final Suggestion suggestion, final DisplayPosition displayPosition, final boolean add) {
        if (suggestion != null && displayPosition != null) {
            Completable.fromCallable((Callable<Boolean>) () -> {
                final SuggestionEntity entity = new SuggestionEntity(Objects.requireNonNull(suggestion.getLocationId()),
                        Objects.requireNonNull(suggestion.getLabel()), Objects.requireNonNull(suggestion.getDistance()),
                        Objects.requireNonNull(suggestion.getMatchLevel()), Objects.requireNonNull(suggestion.getAddress()), displayPosition);
                if (add) {
                    mDatabase.suggestionDao().insert(entity);
                } else {
                    mDatabase.suggestionDao().delete(entity);
                }
                return true;
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorComplete()
                    .subscribe();
        }
    }

}
