package com.critical.here.views.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by dledo on 1/30/18.
 */

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {


    public interface OnRecyclerItemClickListener {
        void onRecyclerItemClick(int position, RecyclerView.ViewHolder viewHolder);
    }

    public interface OnRecyclerItemLongClickListener {
        void onRecyclerItemLongClick(int position, RecyclerView.ViewHolder viewHolder);
    }

    private final List<T> mItems;
    private OnRecyclerItemClickListener onItemClickListener = null;

    protected BaseAdapter() {
        mItems = new ArrayList<>();
        setHasStableIds(true);
    }

    @Override
    public void onBindViewHolder(final VH viewHolder, @SuppressLint("RecyclerView") final int position) {
        viewHolder.itemView.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onRecyclerItemClick(viewHolder.getAdapterPosition(), viewHolder);
            }
        });

        viewHolderBinding(viewHolder, position);
    }

    @Override
    public long getItemId(final int position) {
        return getItem(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onViewDetachedFromWindow(final VH holder) {
        holder.itemView.clearAnimation();
    }

    @Override
    public void onDetachedFromRecyclerView(final RecyclerView recyclerView) {
        mItems.clear();
        onItemClickListener = null;
        super.onDetachedFromRecyclerView(recyclerView);
    }

    //Specific set of methods that should be implemented by subclasses to use sticky headers


    public T getItem(final int index) {

        if (index < mItems.size() && index >= 0) {
            return mItems.get(index);
        }
        return null;
    }

    public List<T> getAllItems() {
        return mItems;
    }

    public void setRecyclerItemClickListener(final OnRecyclerItemClickListener listener) {
        this.onItemClickListener = listener;
    }


    /**
     * Adds all items to the current adapter. This method will clear all older date.
     *
     * @param collection The list of items to be added to this adapter
     */
    public void addAll(final Collection<T> collection) {
        if (collection != null) {
            mItems.clear();
            mItems.addAll(collection);
            notifyDataSetChanged();
        }
    }


    protected abstract void viewHolderBinding(final VH holder, final int position);

}
