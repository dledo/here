package com.critical.here;


import com.critical.here.utils.FragOperation;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayDeque;
import java.util.Queue;


public abstract class BaseActivity extends AppCompatActivity {

    private boolean allowOperations = true;
    private final Queue<FragOperation> mPendingTransactions = new ArrayDeque<>();


    @Override
    protected void onResume() {
        super.onResume();
        allowOperations = true;
        while (mPendingTransactions.size() > 0) {
            executeFragOperation(mPendingTransactions.poll());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        allowOperations = false;
    }


    public void executeFragOperation(final FragOperation operation) {
        if (operation == null) {
            return;
        }
        if (!allowOperations) {
            mPendingTransactions.add(operation);
            return;
        }
        switch (operation.op) {
            case ADD:
                addFragment(operation.fragment, operation.tag, operation.enterAnim, operation.exitAnim);
                break;
            case REPLACE:
                replaceFragment(operation.fragment, operation.tag);
                break;
            case POP:
                popFragment();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!popFragment()) {
            finish();
        }
    }

    private void addFragment(final Fragment fragment, final String tag, final int enterAnim, final int exitAnim) {

        if (fragment != null && findViewById(R.id.here_fragment_container) != null) {
            //Creates a new transaction
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if (enterAnim > 0 && exitAnim > 0) {
                transaction.setCustomAnimations(enterAnim, exitAnim, enterAnim, exitAnim);
            }
            transaction.add(R.id.here_fragment_container, fragment, tag);
            transaction.addToBackStack(tag);
            transaction.commit();
        }
    }

    private void replaceFragment(final Fragment fragment, final String tag) {

        if (fragment != null && findViewById(R.id.here_fragment_container) != null) {
            //Creates a new transaction
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.here_fragment_container, fragment, tag);
            transaction.commit();
        }
    }

    private boolean popFragment() {
        return getSupportFragmentManager().popBackStackImmediate();
    }


    protected boolean isPermissionsGranted(final String... permissions) {
        for (final String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

}
